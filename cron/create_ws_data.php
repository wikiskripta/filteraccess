<?php

/**
 * Get CZ/SK ip addresses from the whole pack
 * @ingroup Extensions
 * @author Josef Martiňák
 */

$datapath = __DIR__ . '/../data';

// get geoname id's for CZ and SK
$countries = [];
$handle = @fopen("$datapath/GeoLite2-Country-Locations-en.csv", 'r');
while(($line = fgets($handle, 4096)) !== false) {
	if(preg_match('/([0-9]*),.*?,(CZ|SK),/', $line, $m)) $countries[$m[2]] = $m[1];
	if(sizeof($countries) == 2) break;
}
fclose($handle);

# IPV4
$handle = fopen("$datapath/GeoLite2-Country-Blocks-IPv4.csv", "r");
$ipv4dataHandle = @fopen("$datapath/ipv4.csv", "w");
while(($line = fgets($handle, 4096)) !== false) {
	$arr = explode(',', rtrim($line));
	foreach($countries as $key=>$value) {
		if($arr[1] == $value) {
			fwrite($ipv4dataHandle, "$arr[0];$key\r\n");
			break;
		}
	}
}
fclose($ipv4dataHandle);
fclose($handle);

#IPV6
$handle = @fopen("$datapath/GeoLite2-Country-Blocks-IPv6.csv", "r");
$ipv6dataHandle = @fopen("$datapath/ipv6.csv", "w");
while(($line = fgets($handle, 4096)) !== false) {
	$arr = explode(',', $line);
	foreach($countries as $key=>$value) {
		if($arr[1] == $value) {
			fwrite($ipv6dataHandle, "$arr[0];$key\r\n");
			break;
		}
	}
}
fclose($ipv6dataHandle);
fclose($handle);

?>
