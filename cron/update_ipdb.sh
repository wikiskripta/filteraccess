# Get IP address database and dig the CZ/SK addresses out
# @ingroup Extensions
# @author Josef Martiňák

# download IP data
curl -o /usr/local/www/nginx/wiki/extensions/FilterAccess/data/ipdb.zip 'https://download.maxmind.com/app/geoip_download?edition_id=GeoLite2-Country-CSV&license_key=M9XlAuy7QHaOYQtt&suffix=zip'
unzip -o /usr/local/www/nginx/wiki/extensions/FilterAccess/data/ipdb.zip -d /usr/local/www/nginx/wiki/extensions/FilterAccess/data
mv /usr/local/www/nginx/wiki/extensions/FilterAccess/data/GeoLite*/* /usr/local/www/nginx/wiki/extensions/FilterAccess/data/
rmdir /usr/local/www/nginx/wiki/extensions/FilterAccess/data/GeoLite*/

# prepare files with CZ  a SK addresses
php /usr/local/www/nginx/wiki/extensions/FilterAccess/cron/create_ws_data.php

# Clean it
rm /usr/local/www/nginx/wiki/extensions/FilterAccess/data/*.txt
rm /usr/local/www/nginx/wiki/extensions/FilterAccess/data/*.zip
