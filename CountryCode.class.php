<?php

/**
 * Class of FilterAccess extension
 * @ingroup Extensions
 * @author Josef Martiňák
 */

class CountryCode {

	public function __construct() {
	}

	public function getLocation() {

		// Get IP
		if(isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
			$explode_ip = explode(",", $_SERVER["HTTP_X_FORWARDED_FOR"]);
			$ccip = $explode_ip[0];
		}
		elseif(isset($_SERVER["REMOTE_ADDR"])) {
			$ccip = $_SERVER["REMOTE_ADDR"];
		}
		else $ccip = "";

		if(substr($ccip,0,3) == "10." || $ccip=="195.113.48.5" || $ccip=="195.113.48.9" || $ccip=="195.113.70.110" || $ccip=="127.0.0.1") {
			return "CZ";	// 1.LF or localhost
		}

		require_once(__DIR__ . '/ip-lib/ip-lib.php');
		if(!empty($ccip)) {
			$address = \IPLib\Factory::addressFromString($ccip);
			if(preg_match('/:/', $ccip)) $dbfile = "ipv6.csv"; else $dbfile = "ipv4.csv";
			$handle = @fopen(__DIR__ . "/data/$dbfile", 'r');
			while(($line = fgets($handle, 4096)) !== false) {
				$arr = explode(";", rtrim($line));
				$range = \IPLib\Factory::rangeFromString($arr[0]);
				if($address->matches($range)) {
					fclose($handle);
					return $arr[1];
					break;
				}
			}
			fclose($handle);
		}
		return 'NOT-CZ-SK';
	}
}
