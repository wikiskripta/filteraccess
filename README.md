# FilterAccess

Get country code of an IP address and refine users' access

## Description

* Extension reads the country code of a visitor and refine users' access.
* Version 1.1

## Installation

* Make sure you have MediaWiki 1.29+ installed.
* Download and place the extension to your /extensions/ folder.
* Add the following code to your LocalSettings.php: 

```php
wfLoadExtension( 'FilterAccess' );
require_once("$IP/extensions/FilterAccess/config/$wgSitename.php");
```

* Set correct paths in _cron/update_ipdb.sh_ and create cron job. Once a week should be enough.

## Configuration

Create file in /config/ folder named _$wgSitename.php_ and refine access here. 
If _CaptchaSettings_ extension installed, the _$countryFilter_ variable (true = allow captcha settings by geolocation) can be used here. Example:

```php
#### DON'T ERASE ####
require_once("$IP/extensions/FilterAccess/CountryCode.class.php");
$cc = new CountryCode();
$countryCode = $cc->getLocation();
#################

if($countryCode!="CZ" && $countryCode!="SK") {
	$wgEmailConfirmToEdit = true;
}
$wgGroupPermissions['*']['skipcaptcha'] = false;
$wgGroupPermissions['user']['skipcaptcha'] = false;
$wgGroupPermissions['autoconfirmed']['skipcaptcha'] = true;
$wgGroupPermissions['bot']['skipcaptcha'] = true; // registered bots
$wgGroupPermissions['sysop']['skipcaptcha'] = true;
if( !empty($countryFilter) && isset($_SERVER["REMOTE_ADDR"]) ){
	$wgCaptchaTriggers['addurl'] = false;
	$wgCaptchaTriggers['edit'] = false;
	if($countryCode=="SK") $wgCaptchaTriggers['addurl'] = true;
	elseif($countryCode!="CZ") $wgCaptchaTriggers['edit'] = true;
}
else $wgCaptchaTriggers['edit'] = false;
```

## Release Notes

### 1.1

* Changing geolocation service to https://www.maxmind.com/

### Third party code

* [ip-lib](https://github.com/mlocati/ip-lib)

## Authors and license

* [Josef Martiňák](https://www.wikiskripta.eu/w/User:Josmart)
* MIT License, Copyright (c) 2023 First Faculty of Medicine, Charles University
